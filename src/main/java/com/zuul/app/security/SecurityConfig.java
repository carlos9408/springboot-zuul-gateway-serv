package com.zuul.app.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobalSecurity(final AuthenticationManagerBuilder auth) throws Exception {
		//TODO crear super admin
		auth.inMemoryAuthentication().withUser("admin").password("{noop}12345").roles("ADMIN");
		auth.inMemoryAuthentication().withUser("frontendapp").password("{noop}12345").roles("USER");
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		//TODO crear acceso al recurso
		http.csrf().disable().authorizeRequests()
			.antMatchers("/zuul-gateway-service/user/primary/**").hasRole("ADMIN")
			.antMatchers("/zuul-gateway-service/user/find/**").hasRole("USER")
			.antMatchers("/zuul-gateway-service/tipoId/**").permitAll()
			.and().httpBasic();
	}

}
